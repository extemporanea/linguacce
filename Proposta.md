---
layout: page
title: Proposta
permalink: /Proposta/
---

## Linguistica per tutte

Anche a voi è arrivata l'eco mediatica sul _linguaggio inclusivo_?
Siete rimaste insoddisfatte da un dibattito dai toni sempre più esagerati, ma non vi accontentate nemmeno di rimanere neutrali?
E poi, soprattutto: cos'è 'sto benedetto _linguaggio_? Lo sappiamo usare, d'accordo; a volte anche molto bene... ma al di là delle norme poco scientifiche che ci hanno inculcato a scuola, sappiamo come funziona?
Dopo le proposte per un linguaggio _inclusivo_ (la più dibattuta è lo schwa, ə), la linguistica sembra essere tornata alla ribalta come strumento politico contro le discriminazioni. Ma come potete vedere dai continui interventi nel dibattito, non esiste _la_ linguistica: esistono _tante_ linguistiche, tante posizioni nel campo scientifico e nel campo politico, spesso in conflitto fra loro. Non sarebbe interessante conoscerle meglio?
Facciamo una proposta _inattuale_: perché non ci leggiamo insieme alcuni testi-chiave della linguistica, continuamente citati ma poco letti? Forse, imparando dalle fonti, potremmo arrivare all'_attualità_ più preparate.
Seguiteci: in quest'avventura potremmo scoprire che non sono poi così lontane le terre in cui non ci sono parole per i numeri dopo il 3, oppure che i saccenti non ci dovrebbero correggere quando non usiamo il congiuntivo...

**I incontro a metà aprile**: lettura commentata di Edward Sapir - Benjamin Lee Whorf, _Linguaggio e relatività_, a cura di M. Carassai e E. Crucianelli, Castelvecchi, Roma 2017

**II incontro a metà maggio**: lettura commentata di Andrea Moro, _La razza e la lingua. Sei lezioni sul razzismo_, La Nave di Teseo, Milano 2019

Se i primi due incontri saranno partecipati, potrebbe esserci un

**III incontro a fine maggio/inizio giugno**: quali dei due libri ci ha convinte di più? Cosa vogliamo tenere di entrambi i testi? Ne potremmo leggere altri? In quali ambiti potremmo applicare le linguistiche (scuola, università, letteratura, politica...)?


Per maggiori informazioni: link al gruppo _Linguaggi_ di eXtemporane su [Element.io](https://matrix.to/#/!PjdBvzFfrOIAFEYMYW:matrix.org?via=matrix.org)
