---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---


In questa libera lettura ci interessiamo di due testi di linguistica:

- _Linguaggio e relatività_, di Edward Sapir e Benjamin Lee Whorf
- _La razza e la lingua. Sei lezioni sul razzismo_, di Andrea Moro


### Date


- **I incontro a metà aprile**: lettura commentata di Edward Sapir - Benjamin Lee Whorf, _Linguaggio e relatività_, a cura di M. Carassai e E. Crucianelli, Castelvecchi, Roma 2017

- **II incontro a metà maggio**: lettura commentata di Andrea Moro, _La razza e la lingua. Sei lezioni sul razzismo_, La Nave di Teseo, Milano 2019


- (possibile) **III incontro a fine maggio/inizio giugno**: confronto e bilancio.
