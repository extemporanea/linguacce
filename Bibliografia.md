---
layout: page
title: Bibliografia
permalink: /bibliografia/
---

# Edizioni di riferimento


- _Linguaggio e relatività_, di Edward Sapir e Benjamin Lee Whorf [CASTELVECCHI](http://www.castelvecchieditore.com/prodotto/linguaggio-e-relativita/)
- _La razza e la lingua. Sei lezioni sul razzismo_, di Andrea Moro [IBS](https://www.ibs.it/razza-lingua-sei-lezioni-sul-libro-andrea-moro/e/9788893446778)
